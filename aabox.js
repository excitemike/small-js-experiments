var aabox = (function () {
'use strict';

// create aabox
function create(upperleft,lowerright) {
    return {type:"aabox",upperleft:vec2.copy(upperleft),lowerright:vec2.copy(lowerright)};
}

// difference between aaboxes
function subtract(a,b){
    return create(
        vec2.subtract(a.upperleft, b.lowerright),
        vec2.subtract(a.lowerright, b.upperleft)
    );
}

// duplicate
function copy(other){
    return create(other.upperleft,other.lowerright);
}

// furthest point on the shape in the direction of the given vector
function support(shape, v){
    var upperleft = shape.upperleft;
    var lowerright = shape.lowerright;
    if (v.x>0){
        if (v.y>0){
            return vec2.copy(lowerright);
        }
        return vec2.create(lowerright.x, upperleft.y);
    }
    if (v.y>0){
        return vec2.create(upperleft.x, lowerright.y);
    }
    return vec2.copy(upperleft);
}

function center(shape){
    return vec2.lerp(shape.upperleft,shape.lowerright,0.5);
}

function translate(shape, v){
    return create(vec2.add(shape.upperleft, v), vec2.add(shape.lowerright, v));
}

/// choose a point in the shape arbitrarily (but consistently)
function arbitraryVertex(shape){
    return shape.upperleft;
}

function draw(ctx,shape,stroke,fill){
    var ul = shape.upperleft;
    var lr = shape.lowerright;
    var x = ul.x;
    var y = ul.y;
    var w = lr.x - x;
    var h = lr.y - y;
    if (fill) {
        ctx.fillStyle = fill;
        ctx.fillRect(x,y,w,h);
    }
    if (stroke) {
        ctx.strokeStyle = stroke;
        ctx.strokeRect(x,y,w,h);
    }
}

// sweep the box into a polygon
function sweep(shape, v){
    var vertices = [];
    var upperleft = shape.upperleft;
    var lowerright = shape.lowerright;
    var left = upperleft.x;
    var top = upperleft.y;
    var right = lowerright.x;
    var bottom = lowerright.y;
    if (v.x>0){
        if (v.y>0){
            return polygon.create([
                vec2.create(left, top),
                vec2.create(right, top),
                vec2.create(right + v.x, top + v.y),
                vec2.create(right + v.x, bottom + v.y),
                vec2.create(left + v.x, bottom + v.y),
                vec2.create(left, bottom),
            ]);
        }
        return polygon.create([
            vec2.create(left, top),
            vec2.create(left + v.x, top + v.y),
            vec2.create(right + v.x, top + v.y),
            vec2.create(right + v.x, bottom + v.y),
            vec2.create(right, bottom),
            vec2.create(left, bottom),
        ]);
    }
    if (v.y>0){
        return polygon.create([
            vec2.create(left, top),
            vec2.create(right, top),
            vec2.create(right, bottom),
            vec2.create(right + v.x, bottom + v.y),
            vec2.create(left + v.x, bottom + v.y),
            vec2.create(left + v.x, top + v.y),
        ]);
    }
    return polygon.create([
        vec2.create(left + v.x, top + v.y),
        vec2.create(right + v.x, top + v.y),
        vec2.create(right, top),
        vec2.create(right, bottom),
        vec2.create(left, bottom),
        vec2.create(left + v.x, bottom + v.y),
    ]);
}

return {
    arbitraryVertex:arbitraryVertex,
    copy:copy,
    create:create,
    draw:draw,
    center:center,
    subtract:subtract,
    support:support,
    sweep:sweep,
    translate:translate
};

}());