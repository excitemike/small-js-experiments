var polygon = (function(){
"use strict";

function copy(other){
    return {type:"polygon",vertices:other.vertices.map(function(v){return vec2.copy(v);})};
}
function create(vertices){
    return {type:"polygon",vertices:vertices};
}
function draw(ctx,shape,stroke,fill){
    ctx.beginPath();
    var vertices = shape.vertices;
    for (var i=0;i<vertices.length;++i) {
        var v = vertices[i];
        if (i===0) {
            ctx.moveTo(v.x,v.y);
        } else {
            ctx.lineTo(v.x,v.y);
        }
    }
    ctx.closePath();
    if (fill) {
        ctx.fillStyle = fill;
        ctx.fill();
    }
    if (stroke) {
        ctx.strokeStyle = stroke;
        ctx.stroke();
    }
}
function support(shape,v){
    var vertices = shape.vertices;
    if (vertices.length === 0){
        return undefined;
    }
    var best_vert = vertices[0];
    var best_dotprod = vec2.dot(vertices[0],v);
    for (var i=1;i<vertices.length;++i){
        var dotprod = vec2.dot(vertices[i],v);
        if (dotprod > best_dotprod) {
            best_dotprod = dotprod;
            best_vert = vertices[i];
        }
    }
    return best_vert;
}
// get a point known to be in the shape, if convex
function center(shape) {
    var vertices = shape.vertices;
    if (vertices.length === 0){
        return undefined;
    }
    var sumx = 0;
    var sumy = 0;
    var count = 0;
    for (var i=0;i<vertices.length;++i){
        sumx += vertices[i].x;
        sumy += vertices[i].y;
        count++;
    }
    return vec2.create(sumx/count, sumy/count);
}

/// choose a point in the shape arbitrarily (but consistently)
function arbitraryVertex(shape){
    var vertices = shape.vertices;
    if (vertices.length === 0){
        return undefined;
    }
    return vertices[0];
}
return {
    arbitraryVertex:arbitraryVertex,
    center:center,
    copy:copy,
    create:create,
    draw:draw,
    support:support
};
}());