// geometry functions not related to a specific type
var geom = (function(){
"use strict";

function closestPointOnLine(segA,segB,closestTo){
    var ox = closestTo.x - segA.x;
    var oy = closestTo.y - segA.y;
    var segx = segB.x - segA.x;
    var segy = segB.y - segA.y;
    var len = sqrt(segx*segx+segy*segy);
    var nx = segx/len;
    var ny = segy/len;
    var dot = ox*nx + oy*ny;
    return vec2.create(segA.x + dot*nx, segA.y + dot*ny);
}
function closestPointOnSegment(segA,segB,closestTo){
    var ox = closestTo.x - segA.x;
    var oy = closestTo.y - segA.y;
    var segx = segB.x - segA.x;
    var segy = segB.y - segA.y;
    var len = sqrt(segx*segx+segy*segy);
    var nx = segx/len;
    var ny = segy/len;
    var dot = ox*nx + oy*ny;
    if (dot < 0) { return segA; }
    if (dot > len) { return segB; }
    return vec2.create(segA.x + dot*nx, segA.y + dot*ny);
}
function distanceToSegment(segA,segB,point){
    var ox = point.x - segA.x;
    var oy = point.y - segA.y;
    var segx = segB.x - segA.x;
    var segy = segB.y - segA.y;
    var len = sqrt(segx*segx+segy*segy);
    var nx = segx/len;
    var ny = segy/len;
    var dot = ox*nx + oy*ny;
    if (dot < 0) { return distance(segA, point); }
    if (dot > len) { return distance(segB, point); }
    var perpX = ox - dot*nx;
    var perpY = oy - dot*ny;
    return Math.sqrt(perpX*perpX + perpY*perpY);
}
function originInTriangle(a,b,c){
    // make it all relative to a
    var b2 = vec2.subtract(b,a);
    var c2 = vec2.distance(c,a);
    var p2 = vec2.scale(a, -1);

    var cc = vec2.dot(c2,c2);
    var bc = vec2.dot(b2,c2);
    var pc = vec2.dot(p2,c2);
    var bb = vec2.dot(b2,b2);
    var pb = vec2.dot(p2,b2);
    var denom = cc*bb - bc*bc;
    var u = (bb*pc - bc*pb) / denom;
    var v = (cc*pb - bc*pc) / denom;

    return (u>=0) && (v>=0) && (u+v < 1);
}

return {
    closestPointOnLine:closestPointOnLine,
    closestPointOnSegment:closestPointOnSegment,
    distanceToSegment:distanceToSegment,
    originInTriangle:originInTriangle
};
}());