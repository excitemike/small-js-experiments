var draw = (function(){
"use strict";

// draw a shape on a canvas context2d
function shape(ctx,shape,stroke,fill){
    if (shape.type==="aabox") {
        aabox.draw(ctx,shape,stroke,fill);
        return;
    }
    if (shape.type==="polygon") {
        polygon.draw(ctx,shape,stroke,fill);
        return;
    }
    throw new Error("draw.shape: unhandled type: " + shape.type);
}

return {
    shape:shape,
};
}());