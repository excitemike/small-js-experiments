var gjk = (function () {
"use strict";

// furthest point on the shape in the direction of the given vector
function support(shape, v){
    if (shape1.type==="aabox") {
        aabox.support(shape, v);
    }
    if (shape1.type==="polygon") {
        polygon.support(shape, v);
    }
    throw new Error("gjk.support: unhandled type: " + shape.type);
}

/// point vaguely in the center of the object
function center(shape, v){
    if (shape1.type==="aabox") {
        aabox.center(shape, v);
    }
    if (shape1.type==="polygon") {
        polygon.center(shape);
    }
    throw new Error("gjk.center: unhandled type: " + shape.type);
}

/// choose a point in the shape arbitrarily (but consistently)
function arbitraryVertex(shape){
    if (shape1.type==="aabox") {
        aabox.arbitraryVertex(shape, v);
    }
    if (shape1.type==="polygon") {
        polygon.arbitraryVertex(shape);
    }
    throw new Error("gjk.arbitraryVertex: unhandled type: " + shape.type);
}

// minkowski difference of two convex shapes
function getMinkowskiDifference(shape1, shape2){
    if ((shape1.type==="aabox") && (shape2.type==="aabox")) {
        return aabox.create(vec2.subtract(shape1.center, shape2.center), vec2.add(shape1.size, shape2.size));
    }
    throw new Error("getMinkowskiDifference: unhandled combination of types: " + shape1.type + ", " + shape2.type);
}

return {
    arbitraryVertex:arbitraryVertex,
    center:center,
    support:support,
    getMinkowskiDifference:getMinkowskiDifference
};

}());