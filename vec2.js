var vec2 = (function () {
'use strict';

// create vec2
function create(x,y) {
    return {x:x,y:y};
}

// add vectors
function add(a,b){
    return create(a.x+b.x,a.y+b.y);
}

// subtract b from a
function subtract(a,b){
    return create(a.x-b.x,a.y-b.y);
}

// duplicate
function copy(v){
    return create(v.x,v.y);
}

// interpolate
function lerp(a,b,t){
    return create((1-t)*a.x + t*b.x, (1-t)*a.y + t*b.y);
}

// multiply by scalar
function scale(v,s){
    return create(s*v.x, s*v.y);
}

// set to unit length without changing direction
function normalize(v){
    var len = length(v);
    return create(v.x/len,v.y/len);
}

function distance(v1,v2){
    return len(subtract(v1,v2));
}

function len(v){
    return Math.sqrt(v.x*v.x + v.y*v.y);
}

return {
    create:create,
    add:add,
    copy:copy,
    distance:distance,
    len:len,
    lerp:lerp,
    normalize:normalize,
    origin:create(0,0),
    scale:scale,
    subtract:subtract
};

}());